import gym
import os
import matplotlib.pylab  as plt
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import random
import torch
import torch.nn as nn
import torch.nn.functional as F

"""definition de la classe DeepRl qui est la classe dans laquelle est definis le modele"""
class DeepRL(nn.Module):
    def __init__(self,dim_etat, dim_hiddenLayer, dim_action,eta ):
        super(DeepRL, self).__init__()
        self.dim_etat = dim_etat
        self.dim_action = dim_action
        self.dim_hiddenLayer = dim_hiddenLayer
        self.eta=eta
        self.model1=nn.Linear(self.dim_etat,self.dim_hiddenLayer)
        self.model2=nn.Linear(self.dim_hiddenLayer,self.dim_action)
        self.loss_fn = torch.nn.MSELoss(reduction='sum')
        self.optimizer = torch.optim.Adam(self.parameters(), lr=self.eta)

    def forward(self,x):
        x=F.relu(self.model1(x))
        y1=self.model2(x)

        return y1


    """Entrainement du modele"""
    def train(self,batch_size, state):
        print("Training")
        if(len(memoire)>batch_size):
            states = []
            targets = []
            batch=tirage(batch_size)
            for observation, action , next_observation, reward, done in batch:
                states.append(state)
                q_values = self(torch.Tensor(observation)).tolist()
                if done:
                    q_values[action] = reward
                else:
                    q_values_next = self(torch.Tensor(next_observation))
                    q_values[action] = reward + torch.max(q_values_next).item()

        targets.append(q_values)


"""renvoie l'action qui a la plus grande Q_valeur"""
def getAction(observation, model, epsilon):
    if random.random() < epsilon:
        action= env.action_space.sample()
    else:
        Q_val= model(torch.Tensor(observation))
        print(Q_val)
        action=action=torch.argmax(Q_val).item()
    return action

"""tirage alléatoire sur la memoire de l'agent """
def tirage(n):
    return random.sample(memoire, n)


if __name__=="__main__":
    env = gym.make('CartPole-v1')
    path_project = os.path.abspath(os.path.join(__file__, ".."))
    path_of_video_with_name = os.path.join(path_project, "videotest.mp4")
    state = env.reset()
    video_recorder = None
    video_recorder = VideoRecorder(env, path_of_video_with_name, enabled=True)
    env.reset()
    print(env.observation_space.shape[0])
    print(env.action_space.n)
    episodeRec={}
    interRec={}
    nb_interactions=0
    memoire=[]
    interactions=()
    epsilon=0.3
    eps_decay=0.99

    model=DeepRL(env.observation_space.shape[0],10,env.action_space.n, 0.05)

    for i_episode in range(20):
        observation = env.reset()

        for t in range(100):
            nb_interactions+=1
            env.render()
            video_recorder.capture_frame()
            print(observation)
            action=getAction(observation,model,epsilon)
            next_observation, reward, done, info = env.step(action)

            interactions=(observation, action,next_observation, reward, done)

            if len(memoire)>100:
                memoire.pop(0)
                memoire.append(interactions)
            else:
                memoire.append(interactions)

            if i_episode not in episodeRec:
                episodeRec[i_episode]=reward
            else:
                episodeRec[i_episode] +=reward

            if done:
                print("Episode finished after {} timesteps".format(t+1))
                interRec[nb_interactions]=episodeRec[i_episode]
                break

            interactions=()
            observation=next_observation
            epsilon=epsilon * eps_decay

    env.reset()


    lists = sorted(interRec.items())
    x, y = zip(*lists)
    plt.xlabel("nombres interactions")
    plt.ylabel("recompenses cumulées")
    plt.text(25,8, 'test')
    plt.title("variations des recompenses cumulées en fonction du nombres d'interactions par episodes")

    plt.plot(x, y)
    plt.show()
    video_recorder.close()
    video_recorder.enabled = False

    env.close()

