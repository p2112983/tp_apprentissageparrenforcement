import gym
import os
import matplotlib.pylab  as plt
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import random
import torch
import torch.nn as nn
import torch.nn.functional as F

if __name__ == '__main__':
    env = gym.make('CartPole-v1')
    path_project = os.path.abspath(os.path.join(__file__, ".."))
    path_of_video_with_name = os.path.join(path_project, "videotest.mp4")
    state = env.reset()
    video_recorder = None
    video_recorder = VideoRecorder(env, path_of_video_with_name, enabled=True)
    env.reset()

    episodeRec={}
    interRec={}
    nb_interactions=0
    memoire=[]
    interactions=()

    for i_episode in range(20):
        observation = env.reset()
        #env.reset()
        for t in range(100):
            nb_interactions+=1
            env.render()
            video_recorder.capture_frame()
            print(observation)

            action = env.action_space.sample()
            observation, reward, done, info = env.step(action)
            if i_episode not in episodeRec:
                episodeRec[i_episode]=reward
            else:
                episodeRec[i_episode] +=reward

            if done:
                print("Episode finished after {} timesteps".format(t+1))
                interRec[nb_interactions]=episodeRec[i_episode]
                break
    env.reset()


    lists = sorted(interRec.items())
    x, y = zip(*lists)
    plt.xlabel("nombres interactions")
    plt.ylabel("recompenses cumulées")
    plt.title("variations des recompenses cumulées en fonction du nombres d'interactions par episodes")

    plt.plot(x, y)
    plt.show()
    video_recorder.close()
    video_recorder.enabled = False

    env.close()



